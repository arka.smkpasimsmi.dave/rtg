<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Company extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company_model', 'company');
    }

    public function index_get()
    {
        $id = $this->get('id');
        if ($id === null) {
            $mahasiswa = $this->company->getCompany();
        } else {
            $mahasiswa = $this->company->getCompany($id);
        }

        if ($mahasiswa) {
            $this->response([
                'status' => true,
                'data' => $mahasiswa
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'id not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => false,
                'message' => 'provide an id'
            ], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            if ($this->company->deleteCompany($id) > 0) {
                //OK
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'Deleted.'
                ], REST_Controller::HTTP_NO_CONTENT);
            } else {
                //id not found
                $this->response([
                    'status' => false,
                    'message' => 'id not found'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_post()
    {
        $data = [
            // 'company_nm' => $this->post('company_nm'),
            // 'description' => $this->post('description')
            'rating' => $this->post('rating')
        ];

        if ($this->company->createCompany($data) > 0) {
            $this->response([
                'status' => true,
                'message' => 'Company Data Added.'
            ], REST_Controller::HTTP_CREATED);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed To Create new Data'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');

        $data = [
            'company_nm' => $this->put('company_nm'),
            'description' => $this->put('description'),
            'rating' => $this->put('rating')
        ];
        if ($this->company->updateCompany($data, $id) > 0) {
            $this->response([
                'status' => true,
                'message' => 'Company Data has been updated.'
            ], REST_Controller::HTTP_NO_CONTENT);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Failed To Create update data'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
