<?php

class Company_model extends CI_Model
{
    public function getCompany($id = null)
    {
        if ($id === null) {
            return $this->db->get('comdata')->result_array();
        } else {
            return $this->db->get_where('comdata', ['id' => $id])->result_array();
        }
    }
    public function deleteCompany($id)
    {
        $this->db->delete('comdata', ['id' => $id]);
        return $this->db->affected_rows();
    }
    public function createCompany($data)
    {
        $this->db->insert('comdata', $data);
        return $this->db->affected_rows();
    }
    public function updateCompany($data, $id)
    {
        $this->db->update('comdata', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
    public function updateRating($data, $id)
    {
        $this->db->update('comdata', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
