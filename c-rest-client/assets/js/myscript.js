const flashData = $('.flash-data').data('flashdata');

if (flashData) {
    Swal({
        title: 'Company Data ',
        text: 'Has' + flashData,
        type: 'success'
    });
}

// tombol-hapus
$('.tombol-hapus').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal({
        title: 'Are You Sure?',
        text: "Company data will be deleted",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Delete Data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })

});