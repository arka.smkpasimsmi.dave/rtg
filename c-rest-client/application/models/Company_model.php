<?php

use  GuzzleHttp\Client;

class Company_model extends CI_model
{
    private $_client;

    public function __construct()
    {
        $this->_client = new Client([
            'base_uri' => 'http://localhost/rest-api/c-rest-server/api/'
        ]);
    }
    public function getAllCompany()
    {
        // return $this->db->get('mahasiswa')->result_array();

        $response = $this->_client->request('GET', 'company');

        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'];
    }
    public function getCompanyById($id)
    {

        $response = $this->_client->request('GET', 'company', [
            'query' => [
                'id' => $id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'][0];
    }

    public function tambahDataCompany()
    {
        $data = [
            // "company_nm" => $this->input->post('company_nm', true),
            // "description" => $this->input->post('description', true)
            "rating" => $this->input->post('rating', true)
        ];

        // $this->db->insert('mahasiswa', $data);
        $response = $this->_client->request('POST', 'company', [
            'form_params' => $data
        ]);
        $result = json_decode($response->getBody()->getContents(), true);

        return $result;
    }

    public function hapusDataCompany($id)
    {
        // $this->db->where('id', $id);
        // $this->db->delete('mahasiswa', ['id' => $id]);
        $response = $this->_client->request('DELETE', 'company', [
            'form_params' => [
                'id' => $id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }


    public function ubahDataCompany()
    {
        $data = [
            "id" => $this->input->post('id', true),
            "company_nm" => $this->input->post('company_nm', true),
            "description" => $this->input->post('description', true),
            "rating" => ""
        ];

        $response = $this->_client->request('PUT', 'company', [
            'form_params' => $data
        ]);
        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'][0];
    }

    public function cariDataCompany()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nama', $keyword);
        $this->db->or_like('jurusan', $keyword);
        $this->db->or_like('nrp', $keyword);
        $this->db->or_like('email', $keyword);
        return $this->db->get('mahasiswa')->result_array();
    }
    public function updateRating()
    {
        $data = [
            "id" => $this->input->post('id', true),
            "company_nm" => $this->input->post('company_nm', true),
            "description" => $this->input->post('description', true),
            "rating" => $this->input->post('rating', true)
        ];

        $response = $this->_client->request('PUT', 'company', [
            'form_params' => $data
        ]);
        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'][0];
    }
}
