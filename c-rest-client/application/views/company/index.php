<div class="container">
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <?php if ($this->session->flashdata('flash')) : ?>
        <!-- <div class="row mt-3">
        <div class="col-md-6">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Data mahasiswa <strong>berhasil</strong> <?= $this->session->flashdata('flash'); ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div> -->
    <?php endif; ?>

    <div class="row mt-3">
        <div class="col-md-6">
            <a href="<?= base_url(); ?>Company/tambah" class="btn btn-primary ml-4" style="font-family: 'Poppins', sans-serif;">Add Rating</a>
        </div>
    </div>

    <!-- <div class="row mt-3">
        <div class="col-md-6">
            <form action="" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari data mahasiswa.." name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Cari</button>
                    </div>
                </div>
            </form>
        </div>
    </div> -->
    <div class="container">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="company_nm">Company Code</label>
                            <input type="date" id="posSearch">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-12 mt-3">
                <h3 style="font-family: 'Poppins', sans-serif;font-weight:bold;color:slategrey;" class="mb-4">Rating List</h3>
                <?php if (empty($mahasiswa)) : ?>
                    <div class="alert alert-danger" role="alert">
                        Company Data not Found.
                    </div>
                <?php endif; ?>
                <ul class="list-group" id="tbody">
                    <?php foreach ($mahasiswa as $mhs) : ?>
                        <li class="list-group-item">
                            <h6 class="bold" style="font-family: 'Poppins', sans-serif;"><?= $mhs['company_nm']; ?></h6>
                            <a href="<?= base_url(); ?>Company/hapus/<?= $mhs['id']; ?>" class="btn btn-danger float-right tombol-hapus"><i class="far fa-trash-alt"></i></a>
                            <a href="<?= base_url(); ?>Company/ubah/<?= $mhs['id']; ?>" class="btn btn-success float-right mr-2 ml-2"><i class="fas fa-pencil-alt"></i></a>
                            <a href="<?= base_url(); ?>Company/detail/<?= $mhs['id']; ?>" class="btn btn-primary float-right"><i class="fas fa-info-circle"></i></a>
                            <a href="<?= base_url(); ?>Company/ubahRating/<?= $mhs['id']; ?>" class="btn btn-warning float-right mr-2 ml-2"><i class="far fa-star"></i></a>
                            <div class="form-group rating">
                                <input type="hidden" name="rating" class="form-control" id="rating" value="<?= $mhs['rating']; ?>">
                                <label>
                                    <input id="bro" type="radio" disabled <?php if ($mhs['rating'] == '1') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <?php if (empty($mhs['rating'])) : ?>
                                        <p style="font-size: 10px;color:red;">
                                            Not Rated!.
                                        </p>
                                    <?php endif; ?>
                                </label>
                                <label>
                                    <input id="bro" type="radio" disabled <?php if ($mhs['rating'] == '2') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <?php if (empty($mhs['rating'])) : ?>
                                        <p style="font-size: 10px;color:red;">
                                            Not Rated!.
                                        </p>
                                    <?php endif; ?>
                                </label>
                                <label>
                                    <input id="bro" type="radio" disabled <?php if ($mhs['rating'] == '3') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <?php if (empty($mhs['rating'])) : ?>
                                        <p style="font-size: 10px;color:red;">
                                            Not Rated!.
                                        </p>
                                    <?php endif; ?>
                                </label>
                                <label>
                                    <input id="bro" type="radio" disabled <?php if ($mhs['rating'] == '4') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <?php if (empty($mhs['rating'])) : ?>
                                        <p style="font-size: 10px;color:red;">
                                            Not Rated!.
                                        </p>
                                    <?php endif; ?>
                                </label>
                                <label>
                                    <input id="bro" type="radio" disabled <?php if ($mhs['rating'] == '5') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <?php if (empty($mhs['rating'])) : ?>
                                        <p style="font-size: 10px;color:red;">
                                            Not Rated!.
                                        </p>
                                    <?php endif; ?>
                                </label>
                            </div>
                            <h6 class="card-title mb-2"><?= $mhs['created_dt']; ?></h6>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    </div>
</div>