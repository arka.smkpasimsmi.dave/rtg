<div class="container">

    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                <div class="card-header">
                    Add Rating
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <!-- <div class="form-group">
                            <h6 for="company_nm">Company Name</h6>
                            <input type="text" name="company_nm" class="form-control" id="company_nm">
                            <small class="form-text text-danger"><?= form_error('company_nm'); ?></small>
                        </div>
                        <div class="form-group">
                            <h6 for="description">Description</h6>
                            <input type="text" name="description" class="form-control" id="description">
                            <small class="form-text text-danger"><?= form_error('description'); ?></small>
                        </div> -->
                        <h6>Give a Rating</h6>
                        <div class="form-group rating">
                            <label>
                                <input type="radio" name="rating" value="1" />
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="2" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="3" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="4" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                            <label>
                                <input type="radio" name="rating" value="5" />
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                                <span class="icon">★</span>
                            </label>
                        </div>
                        <br>
                        <br>
                        <button type="submit" name="tambah" class="btn btn-primary float-right">Add Rating +</button>
                    </form>
                </div>
            </div>


        </div>
    </div>

</div>