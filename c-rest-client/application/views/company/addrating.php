<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bintang.css">
<div class="container">

    <div class="row mt-5">
        <div class="col-md-12">
            <h2 style="font-family: 'Poppins', sans-serif;" class="text-center mb-5 mt-3">Update Company Rating</h2>

            <div class="card mx-auto" style="font-family: 'Poppins', sans-serif;width:50%;">
                <div class="card-header">
                    Change Company Data
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <input type="hidden" name="id" value="<?= $mahasiswa['id']; ?>">
                        <div class="form-group">
                            <input type="text" name="company_nm" class="form-control" id="company_nm" value="<?= $mahasiswa['company_nm']; ?>" hidden>
                            <small class="form-text text-danger"><?= form_error('company_nm'); ?></small>
                        </div>
                        <div class="form-group">
                            <input type="text" name="description" class="form-control" id="description" value="<?= $mahasiswa['description']; ?>" hidden>
                            <small class="form-text text-danger"><?= form_error('description'); ?></small>
                        </div>
                        <div class="form-group rating">
                            <h6 for="nrp">Rating</h6>
                            <input type="hidden" name="rating" class="form-control" id="rating" value="<?= $mahasiswa['rating']; ?>">
                            <div class="form-group rating">
                                <label>
                                    <input type="radio" name="rating" value="1" <?php if ($mahasiswa['rating'] == '1') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="2" <?php if ($mahasiswa['rating'] == '2') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="3" <?php if ($mahasiswa['rating'] == '3') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="4" <?php if ($mahasiswa['rating'] == '4') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                                <label>
                                    <input type="radio" name="rating" value="5" <?php if ($mahasiswa['rating'] == '5') echo "checked='checked'"; ?> />
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                    <span class="icon">★</span>
                                </label>
                            </div>
                        </div>
                </div>
                <button type="submit" name="ubah" class="btn btn-primary text-center">Update</button>
                </form>
            </div>
        </div>


    </div>
</div>

</div>