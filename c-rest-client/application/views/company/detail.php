<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                <div class="card-header">
                    Company Details
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $mahasiswa['company_nm']; ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?= $mahasiswa['description']; ?></h6>
                    <div class="form-group rating" id="wrating">
                        <input type="hidden" name="rating" class="form-control" id="myInput" value="<?= $mahasiswa['rating']; ?>">
                    </div>
                </div>
                <a href="<?= base_url(); ?>mahasiswa" class="btn btn-primary">Back</a>
            </div>
        </div>

    </div>
</div>
</div>