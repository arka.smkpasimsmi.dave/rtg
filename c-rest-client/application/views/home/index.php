<div class="container">
    <div class="row">
        <?php foreach ($mahasiswa as $mhs) : ?>
            <div class="col-md-4 mt-3">
                <div class="card">
                    <img class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?= $mhs['company_nm']; ?></h5>
                        <p class="card-text"><?= $mhs['description']; ?></p>
                        <div class="form-group rating" id="wrating">
                            <input type="hidden" name="rating" class="form-control" id="twoInput" value="<?= $mhs['rating']; ?>">
                        </div>
                        <!-- <a href="<?= base_url(); ?>mahasiswa/detail/<?= $mhs['id']; ?>" class="badge badge-primary float-right">detail</a> -->
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>