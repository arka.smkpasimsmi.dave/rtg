<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Fontawesome -->
  <script src="https://kit.fontawesome.com/2592256b07.js" crossorigin="anonymous"></script>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- My CSS -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">

  <!-- Star CSS -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bintang.css">

  <!-- SB Admin 2 -->
  <link href="<?= base_url('assets/css/sb-admin-2.css') ?>" rel="stylesheet">

  <!-- Datatables -->
  <link href="<?= base_url('assets/vendor/datatables/dataTables.bootstrap4.css') ?>" rel="stylesheet">

  <!-- Poppins Font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

  <title><?php echo $judul; ?>
  </title>
</head>

<body>

  <body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">