</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
   <div class="container my-auto">
      <div class="copyright text-center my-auto">
         <span>Copyright © Your Website 2019</span>
      </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
</div>
</div>

</body>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
<script src="<?= base_url(); ?>assets/js/nscript.js"></script>
<!-- Core Plugin Javascript -->
<script src="<?= base_url('assets/js/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/js/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/js/sb-admin-2.min.js') ?>"></script>

<!-- DataTables -->
<script src="<?= base_url('assets/js/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap4.js') ?>"></script>

<script>
   $(document).ready(function() {
      var str = $("#myInput").val();
      if (str == 1) {
         $('#wrating').html(`<label>
               <span class="icon" style="color:#0099ff;">★</span>
            </label>`)
      }
      if (str == 2) {
         $('#wrating').html(`<label>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
            </label>`)
      }
      if (str == 3) {
         $('#wrating').html(`<label>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
            </label>`)
      }
      if (str == 4) {
         $('#wrating').html(`<label>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
            </label>`)
      }
      if (str == 5) {
         $('#wrating').html(`<label>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
               <span class="icon" style="color:#0099ff;">★</span>
            </label>`)
      }

      $("#posSearch").on("change", function() {
         var value = $(this).val().toLowerCase();
         $("#tbody").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
         });
      });

   });
</script>
</body>

</html>