<?php

class Company extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['judul'] = 'Company List';
        $data['mahasiswa'] = $this->Company_model->getAllCompany();
        if ($this->input->post('keyword')) {
            $data['mahasiswa'] = $this->Company_model->cariDataCompany();
        }
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/navbar');
        $this->load->view('company/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['judul'] = 'Add Rating';

        // $this->form_validation->set_rules('company_nm', 'Company_nm', 'required');
        // $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('rating', 'Rating', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('company/tambah');
            $this->load->view('templates/footer');
        } else {
            $this->Company_model->tambahDataCompany();
            $this->session->set_flashdata('flash', 'Added');
            redirect('company');
        }
    }

    public function hapus($id)
    {
        $this->Company_model->hapusDataCompany($id);
        $this->session->set_flashdata('flash', 'Deleted!');
        redirect('company');
    }

    public function detail($id)
    {
        $data['judul'] = 'Detail Data Mahasiswa';
        $data['mahasiswa'] = $this->Company_model->getCompanyById($id);
        $this->load->view('templates/header', $data);
        $this->load->view('company/detail', $data);
        $this->load->view('templates/footer');
    }

    public function ubah($id)
    {
        $data['judul'] = 'Form Ubah Data Mahasiswa';
        $data['mahasiswa'] = $this->Company_model->getCompanyById($id);

        $this->form_validation->set_rules('company_nm', 'Company_nm', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('company/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Company_model->ubahDataCompany();
            $this->session->set_flashdata('flash', 'Changed!');
            redirect('company');
        }
    }
    public function ubahRating($id)
    {
        $data['judul'] = 'Change Rating';
        $data['mahasiswa'] = $this->Company_model->getCompanyById($id);

        $this->form_validation->set_rules('company_nm', 'Company_nm', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('company/addrating', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Company_model->updateRating();
            $this->session->set_flashdata('flash', 'Changed!');
            redirect('company');
        }
    }
}
